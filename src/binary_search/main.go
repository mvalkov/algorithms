package main

import (
	"flag"
	"fmt"
	"time"
)

var (
	size  = flag.Int("size", 1_000_000, "-size <number>")
	query = flag.Int("query", 355_000, "-query <number>")
)

func main() {
	flag.Parse()

	data := generateDataArray(*size)

	stupidSearch(data, *query)
	binarySearch(data, *query)
}

// Метрика
//
// Принимает:
//     время начала замера - time.Now()
//     текстовую метку
//     указатель на счетчик
//
// Результатом работы будет подобный вывод:
//     Тупой поиск: 38.828215ms. Шагов: 12000001
//
func benchmark(startTime time.Time, title string, counter *int) {
	td := time.Since(startTime)
	fmt.Printf("%v: %v. Шагов: %v\n", title, td, *counter)
}

// Генератор сортированых данных для поиска
//
// Принимает:
//     количество элементов массива
// Возвращает:
//     указатель на массив с сортированными данными
//
func generateDataArray(size int) *[]int {

	defer benchmark(time.Now(), "Генерация данных", &size)

	a := make([]int, size)
	for i := 0; i < size; i++ {
		a[i] = i
	}

	return &a
}

// Поиск перебором.
//
// Принимает:
//     указатель на массив чисел;
//     число, индекс которого надо найти.
//
// Возвращает:
//     индекс найденного числа.
//
func stupidSearch(array *[]int, query int) (index int) {
	counter := 0
	index = -1

	defer benchmark(time.Now(), "Поиск перебором", &counter)

	for idx, val := range *array {
		counter++
		if val == query {
			index = idx
			break
		}
	}

	return index
}

// Двоичный поиск
//
// Принимает:
//     указатель на массив чисел;
//     число, индекс которого надо найти.
//
// Возвращает:
//     индекс найденного числа.
//
func binarySearch(array *[]int, query int) (index int) {
	counter := 0
	index = -1

	defer benchmark(time.Now(), "Бинарный поиск", &counter)

	low := 0
	high := len(*array) - 1
	mid := 0
	guess := 0

	for low <= high {

		counter++

		mid = (low + high) / 2
		guess = (*array)[mid]

		if guess == query {
			index = mid
			break
		}

		if guess > query {
			high = mid - 1
		} else {
			low = mid + 1
		}
	}

	return index
}

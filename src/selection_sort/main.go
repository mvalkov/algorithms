package main

import (
	"crypto/rand"
	"flag"
	"fmt"
	"math/big"
	"sort"
	"time"
)

var (
	size = flag.Int("size", 100, "-size <number>")
)

func main() {
	flag.Parse()

	data1 := generateDataArray(*size)
	data2 := generateDataArray(*size)
	defaultSort(data1)
	selectionSort(data2)
}

// Метрика
//
// Принимает:
//     время начала замера - time.Now()
//     текстовую метку
//     указатель на счетчик
//
// Результатом работы будет подобный вывод:
//     Генерация данных: 2.343772739s. Шагов: 1000000
//
func benchmark(startTime time.Time, title string, counter *int) {
	td := time.Since(startTime)
	fmt.Printf("%v: %v. Шагов: %v\n", title, td, *counter)
}

// Генератор случайных данных для сортировки
//
// Принимает:
//     количество элементов массива
// Возвращает:
//     указатель на массив со случайными числами
//
func generateDataArray(size int) *[]int64 {

	defer benchmark(time.Now(), "Генерация данных", &size)

	arr := make([]int64, size)
	var randInt *big.Int
	for i := 0; i < len(arr); i++ {
		randInt, _ = rand.Int(rand.Reader, big.NewInt(9_999_999))
		arr[i] = randInt.Int64()
	}

	return &arr
}

// Стандартная сортировка для проверки
// использует алгоритм быстрой сортировки
func defaultSort(array *[]int64) {

	counter := 0
	defer benchmark(time.Now(), "Стандартная сортировка", &counter)

	sort.Slice(*array, func(i, j int) bool { counter++; return (*array)[i] < (*array)[j] })
}

// Поиск наименьшего числа в массиве
func findSmallest(array *[]int64, counter *int) (index int) {
	smallest := (*array)[0]
	index = 0

	for i, v := range *array {

		*counter++
		if v < smallest {
			smallest = v
			index = i
		}
	}

	return index
}

// Сортировка выбором
func selectionSort(array *[]int64) {

	counter := 0
	defer benchmark(time.Now(), "Сортировка выбором", &counter)

	sortedArray := []int64{}
	smallestIndex := 0

	for range *array {
		counter++
		smallestIndex = findSmallest(array, &counter)
		sortedArray = append(sortedArray, (*array)[smallestIndex])
		*array = append((*array)[:smallestIndex], (*array)[smallestIndex+1:]...)
	}
	*array = sortedArray
}

# Алгоритмы

Этот проект содержит в себе реализации алгоритмов на Golang.

---

## Двоичный поиск

[Статья в блоге](https://walkit.tk/binarnyi-poisk/)

```bash
cd algorithms
go build -o bin/binary_search.run src/binary_search/main.go && ./bin/binary_search.run -size 45000000 -query 12000000
```

Пример вывода работы программы
```
Генерация данных: 173.696485ms. Шагов: 45000000
Тупой поиск: 42.916062ms. Шагов: 12000001
Двоичый поиск: 683ns. Шагов: 26
```

---

## Сортировка выбором

[Статья в блоге]()

```bash
cd algorithms
go build -o bin/selection_sort.run src/selection_sort/main.go && ./bin/selection_sort.run -size 1000
```
Пример вывода работы программы
```
Генерация данных: 2.231623ms. Шагов: 1000
Генерация данных: 2.437618ms. Шагов: 1000
Стандартная сортировка: 197.228µs. Шагов: 9871
Сортировка выбором: 1.866131ms. Шагов: 501500
```